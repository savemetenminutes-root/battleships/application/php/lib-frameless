<?php

namespace Smtm\Frameless\Router;

use Psr\Http\Message\RequestInterface;

interface RouterInterface
{
    public function match(
        RequestInterface $request,
        $routes = null,
        $requestUriPathSegments = [],
        &$routeStack = [],
        $level = 0
    );
}
