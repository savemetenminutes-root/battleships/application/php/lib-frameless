<?php

namespace Smtm\Frameless\Router;

use Psr\Http\Message\RequestInterface;

class Router implements RouterInterface
{
    protected $routes = [];

    public function __construct($routes)
    {
        $this->routes = $this->load($routes);
    }

    public function load($routes)
    {
        $routesInstances = [];
        foreach ($routes as $name => $route) {
            $children = [];
            if (array_key_exists('children', $route)) {
                $children = $this->load($route['children']);
            }
            $routesInstances[$name] =
                new Route(
                    $route['method'] ?? null,
                    $route['path'] ?? null,
                    $route['action'] ?? null,
                    $children
                );
        }

        return $routesInstances;
    }

    public function match(
        RequestInterface $request,
        $routes = null,
        $requestUriPathSegments = [],
        &$routeStack = [],
        $level = 0
    ) {
        if ($level === 0) {
            $routes = $this->getRoutes();
        }
        foreach ($routes as $name => $route) {
            if ($level === 0) {
                $routeStack             = [];
                $requestUriPathSegments = explode('/', $request->getUri()->getPath());
                array_shift($requestUriPathSegments);
                $currentRequestUriPathSegment = null;
            }

            $match = true;
            if ($route->getPath() !== null) {
                $currentRequestUriPathSegment = array_shift($requestUriPathSegments);
                if ($route->getPath() !== $currentRequestUriPathSegment) {
                    $match = false;
                }
            }
            if ($route->getMethod() !== null) {
                if ($route->getMethod() !== $request->getMethod()) {
                    $match = false;
                }
            }
            if ($match) {
                $routeStack[$name] = $route;
                if (!empty($route->getChildren())) {
                    return $this->match($request, $route->getChildren(), $requestUriPathSegments,
                        $routeStack, $level + 1);
                } else {
                    if (empty($requestUriPathSegments)) {
                        return $routeStack;
                    } else {
                        return false;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }
}
