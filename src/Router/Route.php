<?php

namespace Smtm\Frameless\Router;

class Route
{
    /**
     * @var string|null
     */
    protected $method;
    /**
     * @var string|null
     */
    protected $path;
    /**
     * @var array|callable|null
     */
    protected $action;
    /**
     * @var array|null
     */
    protected $children;

    /**
     * Route constructor.
     * @param string|null $method
     * @param string|null $path
     * @param array|callable|null $action
     * @param array|null $children
     */
    public function __construct(
        ?string $method = null,
        ?string $path = null,
        $action = null,
        ?array $children = null
    ) {
        $this->method   = $method;
        $this->path     = $path;
        $this->action   = $action;
        $this->children = $children;
    }

    /**
     * @return string|null
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @return array|callable|null
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return array|null
     */
    public function getChildren(): ?array
    {
        return $this->children;
    }
}
