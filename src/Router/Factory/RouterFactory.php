<?php

namespace Smtm\Frameless\Router\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Frameless\Router\Router;
use Smtm\Psr\Container\Factory\FactoryInterface;

class RouterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $configRoutes = $container->get('config')['routes'] ?? [];
        return new Router($configRoutes);
    }
}
