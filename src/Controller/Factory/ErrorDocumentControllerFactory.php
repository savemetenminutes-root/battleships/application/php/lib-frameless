<?php

namespace Smtm\Frameless\Controllers\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Frameless\ErrorDocumentController;
use Smtm\Psr\Container\Factory\FactoryInterface;

class ErrorDocumentControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ErrorDocumentController();
    }
}
